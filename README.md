PRTG Device Template for HPE Aruba Wireless devices
===========================================

This project contains all the files necessary to integrate the Aruba
into PRTG for auto discovery and sensor creation.

Please use this link to download [Aruba Wireless Template Install package](https://gitlab.com/PRTG/Device-Templates/Aruba/-/jobs/artifacts/master/download?job=PRTGDistZip)

Installation Instructions
=========================
You can unzip the content of the installation package directly into the PRTG Executable directory.


The template project has a standard directory structure:
[All the files in the PRTG subdirectory needs to go into the PRTG program directory](https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data). 

The templates was designed and tested with  the following devices:
* Aruba WLSX Switch (a7210)
* ArubaOS Aruba3200

Sensor descriptions
====

Template: wLAN Aruba AI/AP (Aruba_AIAP.odt)
------
Access Point Status:
Different variations depending upon variation of device.
![Aruba AP Sensor](./Images/ArubaAIAP_AP_Status.png)
![Aruba AP Sensor](./Images/ArubaAIAP_AP_Status2.png)


Template: wLAN Aruba, Detail Traffic (wLAN Aruba Detail.odt)
------

WLAN station :
![Aruba WLAN station Sensor](./Images/Aruba_wLAN_Station.png)

Access Point Status:
![Aruba Access Point Status](./Images/Aruba_wlsx_APStatus.png)

AP Memory utilization:

![Aruba AP Mem_util Sensor](./Images/Aruba_AP_MemUtil.png)

Access Point Radio:
![Aruba Access Point Radio](./Images/ArubaAIAP_AP_Radio.png)


Template: wLAN Aruba, Detail User Tables (Aruba_UserTable.odt)
------
User table:
![Aruba User Table Sensor](./Images/Aruba_UsrDetail_User.png)
